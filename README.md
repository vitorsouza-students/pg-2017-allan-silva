# README #

Projeto de Graduação de Allan Araujo Silva: _C2D - Módulo de Credenciamento e Classificação de Docentes do Sistema Marvin_.

### Resumo ###

O Programa de Pós-Graduação em Informática da Universidade Federal do Espírito Santo (PPGI/UFES) necessita de um sistema/módulo para manipular dados de docentes, como produções bibliográficas --- publicações de artigos científicos em diversos tipos de veículos (conferências e periódicos) ---, pois atualmente a Coordenação de Aperfeiçoamento de Pessoal de Nível Superior (CAPES) realiza avaliações dos PPGs de acordo com tais informações. Em consequência disso, o PPGI utiliza estes mesmos indicadores de produção como critério de credenciamento e recredenciamento de docentes no programa. Assim, a proposta de um módulo vem para otimizar o processo de extração de dados dos docentes e automatizar o processo de credenciamento e classificação destes docentes no PPGI/UFES.

No desenvolvimento deste módulo, foram seguidos os processos de Engenharia de Software realizando levantamento de requisitos, especificação de requisitos, definição da arquitetura do sistema, implementação e testes. Foram colocados em prática os conhecimentos adquiridos em disciplinas realizadas no decorrer do curso, tais como Engenharia de Software, Engenharia de Requisitos, Projeto de Sistema de Software, Análise de Objetivos, Programação Orientada a Objetos e Desenvolvimento Web e Web Semântica. Também foram utilizadas técnicas de modelagem e desenvolvimento de sistemas orientado a objetos, destacando o método FrameWeb para projeto de aplicações Web centradas em _frameworks_.

### Atualizações ###

Este repositório contém uma cópia do original em https://github.com/dwws-ufes/2017-C2D à época da conclusão do curso pelo aluno. Caso venha a fazer atualizações no repositório original, este poderá ficar defasado.