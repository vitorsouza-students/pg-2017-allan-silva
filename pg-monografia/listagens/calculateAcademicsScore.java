public List<AcademicScore> calculateAcademicsScore(List<Academic> academics, int startYear, int endYear) throws ScoreSystemNotRegisteredException { 
    try {
        //Retrieve the current scores
        ScoreSystem currentScoreSystem = scoreSystemDAO.retrieveCurrentScoreSystem();
        List<Score> currentScoresList = scoreDAO.retrieveByScoreSystem(currentScoreSystem);
        Map<Qualis, Score> scoreQualisMap = new HashMap<Qualis, Score>();
        for(Score s : currentScoresList) {
            scoreQualisMap.put(s.getQualis(), s);
        }

        List<AcademicScore> academicScoreList = new ArrayList<AcademicScore>();
        for(Academic a : academics) {
            AcademicScore as = new AcademicScore();
            as.setAcademic(a);

            List<Publication> publicationsList = publicationDAO.retrieveByAcademicAndYearRange(a, startYear, endYear);
            List<PublicationScore> publicationScoreList = new ArrayList<PublicationScore>();

            //Stores the academic score in each category
            int scoreConferenceAcademic = 0;
            int scoreJournalAcademic = 0;

            for(Publication p : publicationsList) {
                Venue pubVenue = p.getVenue();

                //No way to calculating the score of publications without associated venues
                if (pubVenue == null) continue;
                try {
                    Qualification quaPubVenue = qualificationDAO.retrieveClosestByVenueAndYear(pubVenue, p.getYear());
                    PublicationScore publicationScore = new PublicationScore();

                    publicationScore.setPublication(p);

                    //Stores the current publication score
                    int currentScore = 0;
                    boolean venueIsConference = pubVenue.getCategory().equals(VenueCategory.CONFERENCE);

                    Qualis qualis = quaPubVenue.getQualis();
                    Score score = scoreQualisMap.get(qualis);
                    publicationScore.setQualis(qualis);

                    if (venueIsConference) {
                        currentScore += score.getScoreConference();
                        scoreConferenceAcademic += currentScore;
                    }
                    else {
                        currentScore += getScoreJournal();
                        scoreJournalAcademic += currentScore;

                    }
                    publicationScore.setScore(currentScore);
                    publicationsList.add(publicationScore);
                }
                catch(PersistentObjectNotFoundException e) {
                    // If there is no qualification that applies to the current publication,
                    // skip this publication and go to the next one;
                    logger.log(Level.WARNING,
                            "No qualification from an year that is less than or equal to the publication's year was found.");					
                }
                catch (MultiplePersistentObjectsFoundException e) {
                    // This is a bug. Log and throw a runtime exception.
                    logger.log(Level.SEVERE, "Multiple qualifications found that have the same year and belong to the same venue.", e);
                    throw new EJBException(e);
                }
            }
            as.setPublicationsScores(publicationScoreList);
            as.setScoreConference(scoreConferenceAcademic);
            as.setScoreJournal(scoreJournalAcademic);
            as.setScoreTotal(scoreConferenceAcademic + scoreJournalAcademic);
            academicScoreList.add(as);
        }
        return academicScoreList;

    } catch (PersistentObjectNotFoundException e) {
        // If there is no Score System that is currently active, throw an
        // exception from the domain.
        logger.log(Level.WARNING,
                "No currently active score system was found.");
        throw new ScoreSystemNotRegisteredException();

    } catch (MultiplePersistentObjectsFoundException e) {
        // This is a bug. Log and throw a runtime exception.
        logger.log(Level.SEVERE, "Multiple score systems found that are currently active.", e);
        throw new EJBException(e);
    }
}