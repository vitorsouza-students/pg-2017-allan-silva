% ===================================================================
% PG - Allan Araujo Silva
% Capítulo 2 - Referencial Teórico
% ===================================================================

\chapter{Referencial Teórico}
\label{sec-referencial}

Neste capítulo, são apresentados os conceitos teóricos fundamentais para o desenvolvimento do sistema C2D, em 3 seções. A Seção~\ref{sec-engsoft} aborda a Engenharia de Software, frisando os principais processos e conceitos utilizados. A Seção~\ref{sec-webdev} apresenta os principais conceitos de desenvolvimento de sistemas na plataforma Web. A Seção~\ref{sec-frameweb} apresenta o método FrameWeb.

\section{Engenharia de Software}
\label{sec-engsoft}

O desenvolvimento de software, de fato, é uma atividade de suma importância para nossa geração. A utilização de computadores e outros dispositivos nas mais diversas áreas do conhecimento humano tem gerado uma crescente demanda por soluções computadorizadas. Buscando melhorar a qualidade dos produtos de software e aumentar a produtividade no processo de desenvolvimento, surgiu a Engenharia de Software~\cite{falboEngSoft}.

Segundo \citeonline{pressman2011engenharia}, a Engenharia de Software proporciona várias ferramentas para o desenvolvimento de um software de qualidade por meio da especificação, desenvolvimento e manutenção destes sistemas de software. Tudo isso é feito por meio de tecnologias e práticas de gerência de projetos e outras disciplinas, visando organização, produtividade e qualidade nesse processo de desenvolvimento~\cite{falboEngSoft}. Sendo assim, podemos citar as seguintes atividades do processo de desenvolvimento de software: elicitação de requisitos (especificação e análise), projeto arquitetural, implementação, análise de riscos, definição de cronogramas, dentre outros.

Como principais atividades no processo de desenvolvimento de software, podemos citar a fase de engenharia de requisitos e a fase de projeto do sistema. Porém, vale ressaltar que, paralelo a estas etapas principais, são realizados diversos processos para auxílio na gerência do projeto, como organização de cronograma, delegação de responsabilidades no projeto, análise dos riscos, testes etc.

Nas seções seguintes, serão apresentadas as atividades referentes a este processo de desenvolvimento de software. Na Seção~\ref{sec-espec-req}, será abordada a etapa de especificação e análise de requisitos. Na seção~\ref{sec-objetivos} será abordada a etapa de análise de objetivos e os conceitos necessários para tal atividade. Na Seção~\ref{sec-proj}, será abordada a etapa de projeto e implementação, sendo levantados conceitos relevantes a fim de explicar e exemplificar de forma clara a importância dessas atividades no contexto do desenvolvimento de software.

\subsection{Engenharia de Requisitos}
\label{sec-espec-req}

A Engenharia de Requisitos é considerada uma das atividades mais importantes
no desenvolvimento de software, pois é nela que são definidas as necessidades do cliente e os possíveis problemas e limitações do projeto.
O desenvolvimento da especificação de requisitos de um software é reconhecido como uma das bases das funcionalidades de um sistema. Estes requisitos são determinantes da qualidade do software, dado que estudos empíricos mostraram que erros nos requisitos são as falhas mais comuns no ciclo de vida de um software, bem como as mais caras e custosas a corrigir~\cite{aurum2013managing}.


Podemos definir um \textbf{requisito} como~\cite{ieee1990standard}:
\begin{enumerate}
	\item Uma condição a ser alcançada por um sistema ou componentes de um sistema a fim de satisfazer um contrato, especificação ou outros documentos de formalização;
	\item Um trabalho necessário para resolver um problema ou alcançar um objetivo;
	\item Uma representação documentada de uma condição ou capacidade, de acordo com (1) e (2) anteriormente descritos;
\end{enumerate}

A atividade inicia com o levantamento de requisitos, onde são coletadas as necessidades dos usuários, informações de domínio, restrições, sistemas interligados, regulamentos etc. A criação de modelos segue em paralelo à atividade de análise, onde estes representam o que o sistema faz. Esta atividade é conhecida como Modelagem Conceitual. Nesta etapa, o foco é o domínio do problema e não a solução técnica, linguagem a ser usada na implementação etc. Com essas informações, jé se pode avançar no processo de desenvolvimento de software~\cite{falboEngReq}.

A Figura~\ref{fig-processo-req} representa o ciclo de vida do processo de Engenharia de Requisitos, identificando suas principais atividades e dependências.

\begin{figure}[h!]
	\centering
	\includegraphics[width=\textwidth]{figuras/referencial/processo-requisitos.png}
	\caption{Processo de Engenharia de Requisitos adaptado de~\cite{kotonya1998requirements}.}
	\label{fig-processo-req}
\end{figure} 

Assim, a especificação dos requisitos e análise das necessidades para o desenvolvimento de um software agrega muitas outras tarefas além de apenas perguntar aos \textit{stakeholders} quais são suas necessidades. Requer uma profunda análise de todo o contexto da organização e do domínio da aplicação com seus processos e regras de negócio.

Para melhor entendimento e visualização, são criados modelos, os quais descrevem as funcionalidades do software, deixando de lado a questão da implementação. Esta fase é chamada de Modelagem Conceitual, enfatizando o domínio do problema e não se deve pensar na solução técnica, computacional que será utilizada. Obtendo os requisitos, mesmo que de forma parcial, e especificando-os na forma de modelos, pode-se  iniciar o trabalho no domínio da solução~\cite{falboEngReq}.

Pode-se dizer que os requisitos de um sistema incluem especificações dos serviços que o sistema deve prover, restrições sob as quais ele deve operar, propriedades gerais do sistema e restrições que devem ser satisfeitas no seu processo de desenvolvimento (FALBO, 2017),
sendo estes requisitos divididos em requisitos funcionais, requisitos não funcionais e regras de negócio, definidos segundo \cite{aurum2013managing} da seguinte forma:

\begin{itemize}
	\item \textbf{Requisitos Funcionais:} o que o sistema efetivamente deve fazer;
	\item \textbf{Requisitos Não-funcionais:} especificação de um determinado comportamento do sistema. Por exemplo: usabilidade, interoperabilidade e segurança;
	\item \textbf{Regras de negócio:} usadas pela organização de forma a satisfazer os objetivos de negócio e clientes, segundo convenções definidas para o negócio.
\end{itemize}


\subsection{Análise de Objetivos}
\label{sec-objetivos}

A análise e modelagem de objetivos utilizando a linguagem iStar 2.0 (anteriormente i*) é uma tentativa de introduzir alguns aspectos de modelagem social e raciocínio em métodos de engenharia em sistemas de informação, especialmente no nível de requisitos. Diferente dos métodos tradicionais de análise de sistemas, em iStar tem-se a capacidade de reconhecimento de relações e intenções de atores no contexto social, focando em como as intenções dos atores (seja humano ou sistema) estão dispostas no domínio, podendo ser feitas alterações ou reconfigurações das relações de modo a ajudar nos objetivos dos atores \cite{Mylopoulos:1999:OGR:291469.293165}.
Para isso, iStar apresenta alguns construtos como: \textit{Actor, Agent, Role, Goal, Quality, Task, Resource}~\cite{DBLP:journals/corr/DalpiazFH16}. Alguns destes construtos são exemplificados na Figura~\ref{fig-istar} e explicados brevemente à seguir:

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.6\textwidth]{figuras/referencial/construtos.png}
	\caption{Construtos da linguagem iStar 2.0.}
	\label{fig-istar}
\end{figure}

\begin{itemize}
	\item \textit{Actor} (Ator): entidade que possui objetivos e realiza ações para alcançá-los, exercendo o seu \textit{know-how};
	
	\item \textit{Goal} (Objetivo): representa o desejo intencional de um ator. Os detalhes de como o objetivo é satisfeito não é descrito pelo objetivo. Portanto, pode ser descrito através de refinamento em tarefas; 
	
	\item \textit{Quality} (Qualidade): é um objetivo que se diferencia do \textit{goal} por seu critério de satisfação, que é subjetivo. Um \textit{quality} é considerado suficientemente satisfeito do ponto de vista do ator;
	
	\item \textit{Task} (Tarefa): uma ação ou um conjunto de ações (passo a passo) realizadas pelo ator. O detalhamento da tarefa pode ser obtido pela decomposição de tarefa em outros subelementos;
	
	\item \textit{Resource} (Recurso): entidade física ou informacional usada pelo ator. Assume-se que o recurso está disponível ao ator que o utiliza.
	
\end{itemize}

Além disso, existem links para representação de relações, descritos à seguir:

\begin{itemize}
	\item \textit{OR-Refiniment link}: aponta para uma relação entre um fim e um meio que atinge esse fim. Um meio pode, por exemplo, ser uma tarefa ou um recurso e um fim pode ser um objetivo;
	
	\item \textit{AND-Refiniment link}: um objetivo pode ser decomposto em subobjetivos e uma tarefa pode ser decomposta em quatro tipos de elementos: um subobjetivo, uma subtarefa, um recurso, e/ou um \textit{quality};
	
	\item \textit{Contribution link}: contribuições podem ser usadas para ligar qualquer um dos elementos a um \textit{quality}, para modelar a maneira que o elemento afeta a satisfação ou cumprimento do \textit{quali}ty;
	
	\begin{itemize}
		\item Make ($++$): a contribuição é positiva o suficiente para satisfazer o \textit{quality}; 
		\item Help ($+$): uma contribuição positiva parcial, não é suficiente por si só para satisfazer o \textit{quality};
		\item Hurt ($-$): uma contribuição negativa parcial, não é suficiente por si só para negar o \textit{quality};
		\item Break ($--$): a contribuição é negativa o suficiente para negar o \textit{quality}.
		
	\end{itemize}
\end{itemize}

Há uma ferramenta online, disponível em \url{http://www.cin.ufpe.br/~jhcp/pistar/}, que permite a criação de diagramas usando a linguagem iStar 2.0.

Em suma, a análise de objetivos consiste em prover a captura das atividades e intenções de determinada organização ou sistema, por exemplo. Tal análise fortalece o alinhamento entre os recursos, requisitos necessários para desenvolvimento do sistema e regras, limitações também existentes, proporcionando uma visão geral do domínio e auxiliando nas decisões a serem tomadas para se chegar ao objetivo final, quer seja aprimorar estratégias em uma organização, quer seja o desenvolvimento de um sistema de forma eficaz.

\subsection{Projeto e Implementação}
\label{sec-proj}

Nesta fase, tem-se por objetivo definir o software a ser implementado. É na fase de projeto que se decide como a solução será implementada, visto que um mesmo problema pode ser resolvido de inúmeras formas possíveis. 
Desta forma, define-se o projeto da arquitetura do sistema, onde é descrita a estrutura de nível mais alto da aplicação, identificando seus elementos ou componentes e suas dependências entre si. A partir daí, é realizado um detalhamento desses elementos, até chegar no nível de unidades de implementação \cite{falboProjeto},  o qual corresponde à primeira atividade, focando nos aspectos tecnológicos, sendo, assim, a fase do processo de software na qual os requisitos, as necessidades do negócio e as considerações técnicas se juntam na formulação de um produto ou sistema de software~\cite{pressman2011engenharia}.

Inicialmente, o projeto é representado em um nível alto de abstração, focando
a estrutura geral do sistema. Definida a arquitetura, o projeto passa a tratar do detalhamento de seus elementos. Esses refinamentos conduzem a representações de menores níveis de abstração, até se chegar ao projeto de algoritmos e estruturas de dados.
Assim, independentemente do paradigma adotado, o processo de projeto envolve as seguintes atividades \cite{falboProjeto}:


\begin{itemize}	
	\item \textbf{Projeto da Arquitetura do Software:} visa definir os elementos estruturais do software e seus relacionamentos.
	\item \textbf{Projeto dos Elementos da Arquitetura:} visa projetar em um maior nível de detalhes cada um dos elementos estruturais definidos na arquitetura, o que envolve a decomposição de módulos em outros módulos menores.
	\item \textbf{Projeto Detalhado:} tem por objetivo refinar e detalhar os elementos mais básicos da arquitetura do software, i.e., as interfaces, os procedimentos e as estruturas de dados. Deve-se descrever como se dará a comunicação entre os elementos da arquitetura (interfaces internas), a comunicação do sistema em desenvolvimento com outros sistemas (interfaces externas) e com as pessoas que vão utilizá-lo (interface com o usuário), bem como se devem projetar detalhes de algoritmos e estruturas de dados. 
\end{itemize}

Ao final da fase de Projeto e Implementação, espera-se que a arquitetura do sistema esteja definida, bem como o projeto de seus componentes, divididos geralmente em camadas a fim de assegurar a separação de funcionalidades pertencentes a nichos diferentes. Embora não seja a única, uma das estruturas mais conhecidas para o projeto de software é a que o divide em camada de apresentação, camada de domínio e camada de persistência~\cite{fowler2002patterns}. No contexto da Web, na camada de \textbf{apresentação}, espera-se lidar com requisições HTTP, exibição de informação e iterações do usuário (cliques de mouse, por exemplo). Na camada de \textbf{domínio}, como o próprio nome já diz, o foco está nas variáveis do domínio do sistema, implementando todas as regras de negócio e requisitos do sistema. Já na camada de \textbf{persistência}, espera-se realizar todo o relacionamento da aplicação com o banco de dados, permitindo armazenar as informações do sistema e recuperá-las quando necessário.

\section{Desenvolvimento Web}
\label{sec-webdev}

A tecnologia está fortemente presente em nosso cotidiano. Um bom exemplo disso são as aplicações Web, executadas em computadores, tablets, smartphones etc, as quais podem ser utilizadas por quaisquer pessoas com acesso à Internet. Com isso, torna-se importante o processo de desenvolvimento de aplicações Web com características como manutenibilidade e escalabilidade.

Pode-se destacar diferenças entre o desenvolvimento de softwares voltados para Web e desenvolvimento de softwares tradicionais (Desktop): são sistemas cujo conteúdo está em constante alteração, buscando ter dados sempre mais atualizados e concisos, além de um rápido crescimento dos requisitos de sistemas na Web. Portanto, tais aplicações precisam ser   cuidadosamente planejadas para suportar escalabilidade e permitir fácil manutenção, além de serem desenvolvidas com enfoque no usuário e possuir um ciclo de vida ágil e uma grande preocupação com acessibilidade, porquanto tais recursos não podem ser adicionados posteriormente ou necessitam de um esforço evitável. O sucesso na construção, desenvolvimento, implementação e manutenção de um sistema Web depende fortemente de quão bem essas questões foram tratadas.

Com o avanço da Web, surgiu a chamada Engenharia Web, que lida com o processo de desenvolvimento de aplicações e sistemas Web. Sua essência é de gerenciar a diversidade e a complexidade dessas aplicações, evitando potenciais falhas que possam ocasionar implicações sérias. Pode-se dizer que a Engenharia Web é uma abordagem proativa de desenvolver aplicações Web~\cite{ginige2001web}.

A Java EE (Java Platform, Enterprise Edition) é uma plataforma padrão para desenvolver aplicações Java robustas e/ou para a Web, incluindo bibliotecas e funcionalidades para implementar softwares Java distribuídos. Se baseia em componentes modulares que executam em servidores de aplicações e que suportam escalabilidade, segurança, integridade e outros requisitos de aplicações corporativas~\cite{thiagoJava}.

A plataforma Java EE possui inúmeras tecnologias com diferentes objetivos, por isso é considerada uma plataforma guarda-chuva. As especificações Java EE mais conhecidas são listadas em~\cite{manzoli-pg16}:

\begin{itemize}
	\item \textbf{Servlets}: são componentes Java executados no servidor para gerar conteúdo dinâmico para a Web, como HTML e XML, por exemplo;
	\item \textbf{JSF (JavaServer Faces)}:  é um \textit{framework} Web baseado em Java com o objetivo de simplificar o desenvolvimento de interfaces de sistemas para a Web, por
	meio de um modelo de componentes as quais podem ser reutilizadas; 
	\item \textbf{JPA (Java Persistence API)}: é uma API padrão do Java para persistência de dados, baseada no conceito de mapeamento objeto/relacional. Essa tecnologia aumenta a produtividade para o desenvolvimento de sistemas onde a integração com banco de dados é necessária; 
	\item \textbf{EJB (Enterprise Java Beans)}: são componentes que executam em servidores de aplicação e possuem como principais objetivos fornecer facilidade e produtividade no
	desenvolvimento de componentes distribuídos, transacionados, seguros e portáveis;
	\item \textbf{CDI (Contexts and Dependency Injection)}: especificação da Java EE que trabalha com injeção de dependências.

\end{itemize}

O Marvin (e, por conseguinte, o módulo C2D) também utiliza uma biblioteca de componentes de interface com o usuário chamada \textit{Primefaces}.\footnote{\url{http://www.primefaces.org/}} O uso desta biblioteca otimiza o tempo gasto no desenvolvimento de componentes web responsivos (tabelas, campos de formulários, botões, dentre outros). Assim, podem ser criadas interfaces mais robustas e agradáveis ao olhar do usuário.

\newpage
\subsection{O Ambiente Web}
\label{sec-ambiente-web}

Para que as aplicações Web sejam executadas corretamente no computador do usuário, existem diversas tecnologias envolvidas neste processo e que se mostram de suma importância para a total compreensão do problema. Desde o surgimento da Web, o projeto evoluiu e são várias as razões para o seu atual sucesso, mas duas merecem destaque: sua arquitetura simples (mas eficiente) e uma interface igualmente simples, originalmente baseada no paradigma de hipertextos.

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.6\textwidth]{figuras/referencial/client-server.png}
	\caption{Representação da Arquitetura de Rede \textit{Cliente-Servidor}}
	\label{fig-client-server}
\end{figure}

Baseado na arquitetura cliente/servidor, o ambiente Web é formado por uma rede de computadores, onde cliente(s) e servidor(es) se comunicam através de requisições e solicitações, como mostra a Figura~\ref{fig-client-server}. Utilizando um \textit{browser} (navegador), o cliente faz as requisições ao servidor --- estas sendo apresentadas para o usuário da aplicação. Já no lado do servidor são recebidas e atendidas as requisições dos clientes.
Os principais componentes desta arquitetura são o \textit{HyperText Transfer Protocol} (HTTP): protocolo de comunicação utilizado para realizar as requisições por parte do usuário e resposta por parte do servidor; \textit{Uniform Resource Locator} (URL): sistema de endereçamento o qual faz  referência a um recurso na Internet; \textit{HyperText Markup Language} (HTML): linguagem de marcação utilizada no desenvolvimento de páginas Web, permitindo criação de documentos que podem ser lidos por qualquer computador;


O \textbf{protocolo HTTP} é por onde os arquivos da Web são transmitidos, executado sobre a camada TCP/IP da Internet. Este protocolo pode ser organizado nos seguintes estados:

\begin{enumerate}[]
	\item \textbf{Conexão}: O cliente estabelece uma conexão com o servidor; 
	\item \textbf{Requisição}: O cliente envia um pedido ao servidor; 
	\item \textbf{Resposta}: O servidor devolve uma resposta ao pedido do cliente;
	\item \textbf{Encerramento}: A conexão é finalizada tanto pelo cliente quanto pelo servidor.
\end{enumerate}

 Quando um documento ou um objeto (como um arquivo de áudio, por exemplo) é enviado para o cliente, é anexado um cabeçalho com a informação necessária para que o \textit{browser} possa interpretá-lo e apresentá-lo ao usuário de forma adequada.

\textbf{URL} é a forma de referenciar objetos na Web. Consiste na identificação do esquema utilizado (HTTP, FTP, dentre outros) junto do caminho até o objeto ou documento desejado, como, por exemplo: \url{http://aluno.ufes.br/}.

Por fim, a linguagem \textbf{HTML} especifica a estrutura e a formatação para documentos do tipo hipertexto por meio de \textit{tags} que indicam como estes devem ser visualizados.

\subsection{MVC - Padrão de Arquitetura de Software}
\label{sec-mvc}

Com o advento da Internet e da procura cada vez maior por sistemas Web, fez-se necessário criar formas de agilizar o desenvolvimento destes sem comprometer a qualidade do produto final. Com isso, surgiu uma série de padrões de arquitetura de software em busca de atributos de qualidade como: (1) descentralização do desenvolvimento por dividir o código da aplicação em vários componentes, possibilitando que os desenvolvedores possam trabalhar paralelamente em diferentes componentes sem que isso cause impactos no que foi desenvolvido por outra pessoa; e (2) fraco acoplamento, uma vez que um dos princípios destes padrões de arquitetura é que suas camadas sejam o mais isoladas possível a fim de que umas não interfiram nas funcionalidades das outras.

Dentre os diversos padrões existentes, um dos mais utilizados é o padrão MVC: \textit{Model-View-Controller}. Este padrão se baseia na divisão da aplicação em três camadas, conforme mostra a Figura~\ref{fig-mvc}. Neste modelo, cada camada possui uma funcionalidade específica:

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.6\textwidth]{figuras/referencial/modelo_mvc.png}
	\caption{Representação da Arquitetura de Software \textit{Model-View-Controller}}
	\label{fig-mvc}
\end{figure}

\begin{itemize}
	\item \textbf{Model:} camada que contém a lógica da aplicação, responsável pelas regras de negócio e pela comunicação da aplicação com o banco de dados;
	\item \textbf{View:} de forma sucinta, é a camada que interage diretamente com o usuário, exibindo dados por meio de arquivos HTML, XML, XHTML, PDF, entre outros;
	\item \textbf{Controller:} camada intermediadora do sistema, comunicando-se com as outras duas camadas. As requisições provenientes do \textit{browser} são processadas pelo \textit{controller}, o qual acessa as informações dos \textit{models} e as retorna para as \textit{views}, exibindo de forma adequada ao usuário.
\end{itemize}


\section{O método FrameWeb}
\label{sec-frameweb}

FrameWeb é um método de projeto para construção de sistemas de informação Web (Web Information Systems – WIS’s) baseado em frameworks. O método define alguns tipos de frameworks que serão usados para construção da aplicação, definindo uma arquitetura básica para o WIS e propõe modelos de projeto que se aproximam da implementação do sistema usando esses \textit{frameworks}~\cite{souza-frameweb07}.

De modo a se integrar bem com os \textit{frameworks} utilizados, o FrameWeb define uma arquitetura lógica padrão para WISs baseada no padrão arquitetônico \textit{Service Layer} (Camada de Serviço)~\cite{fowler2002patterns}. O sistema é dividido em três grandes camadas~\cite{salvatore2016alocaweb}:

\begin{itemize}
	\item\textbf{ Camada de Negócio \textit{(Business Tier)}:} responsável pelas funcionalidades relacionadas
	às regras de negócio da aplicação. Esta camada é particionada em duas: Lógica de	Domínio (\textit{Domain}) e Lógica de Aplicação (\textit{Application});
	\item \textbf{Camada de Apresentação \textit{(Presentation Tier)}:} responsável por funcionalidades de interface com o usuário (incluindo as telas que serão apresentadas a ele). Esta camada, segundo o padrão proposto, é também particionada em duas outras: Visão (\textit{View}) e Controle (\textit{Control});
	\item \textbf{Camada de Acesso a Dados \textit{(Data Access Tier)}:} responsável por funcionalidades
	relacionadas à persistência de dados.
\end{itemize}

Além da definição desta arquitetura padrão, FrameWeb propõe um conjunto de modelos de projeto que trazem conceitos utilizados pelos \textit{frameworks} para esta fase do processo por meio da definição de uma linguagem específica de domínio que faz com que os diagramas fiquem mais próximos da implementação~\cite{souza-frameweb07,martins-mestrado15}.

Para representar componentes típicos da plataforma Web e dos \textit{frameworks} utilizados, o FrameWeb estende o metamodelo da UML, especificando, assim, uma sintaxe própria. Com isso, é possível utilizá-lo para a construção de diagramas de quatro tipos:

\begin{itemize}

	\item \textbf{Modelo de Entidades} (\textit{Entity Model}): é um diagrama de classes da UML que representa os objetos de	domínio do problema e seu mapeamento para a persistência em banco de dados relacional;
	\item \textbf{Modelo de Persistência} (\textit{Persistence Model}): é um diagrama de classes da UML que representa as classes de acesso a dados (DAO) existentes, responsáveis pela persistência das instâncias das classes de domínio;
	\item \textbf{Modelo de Navegação} (\textit{Navigation Model}): é um diagrama de classe da UML que representa os diferentes componentes que formam a camada de Lógica de Apresentação, como páginas Web, formulários HTML e \textit{controllers};
	\item \textbf{Modelo de Aplicação} (\textit{Application Model}): é um diagrama de classes da UML que representa as classes de	serviço, que são responsáveis pela codificação dos casos de uso, e suas dependências;
\end{itemize}
%
%\begin{figure}[h!]
%	\centering
%	\includegraphics[width=0.5\textwidth]{figuras/referencial/modelos_frameweb.png}
%	\caption{Modelos propostos pelo FrameWeb}
%	\label{fig-modelo_fw}
%\end{figure}
