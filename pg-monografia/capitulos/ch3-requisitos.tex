% ===================================================================
% PG - Allan Araujo Silva
% Capítulo 3 - Especificação de Requisitos
% ===================================================================

\chapter{Especificação de Requisitos}
\label{sec-requisitos}

Neste capítulo são abordados os resultados obtidos na fase de Engenharia de Requisitos para a construção do sistema de Classificação e Credenciamento de Docentes (C2D). Na Seção~\ref{sec-requisitos-escopo}, é apresentado o minimundo do projeto; na Seção~\ref{sec-analise-objetivo} são analisados os objetivos da aplicação a ser construída; na Seção~\ref{sec-requisitos-casos-de-uso}, são apresentados diagramas de casos de uso e na Seção~\ref{sec-requisitos-diagrama-de-classes}, são apresentados os diagramas de classes. Os requisitos funcionais,
requisitos não funcionais e regras de negócio estão localizados no Documento de Especificação de Requisitos, disponível no Apêndice ao final desta monografia.


\section{Descrição do Escopo do Projeto}
\label{sec-requisitos-escopo}

Programas de Pós-Graduação (PPGs) são avaliados pela CAPES (Coordenação de Aperfeiçoamento de Pessoal de Nível Superior) de acordo com sua produção técnica, ou seja, publicação de artigos científicos em veículos (conferências e periódicos) qualificados. Por esta razão, os PPGs elaboram critérios para que docentes sejam credenciados no programa, se mantenham dentro do programa, alcancem diferentes níveis dentro do programa, etc. Todos estes critérios giram em torno das publicações dos professores e da qualificação que a CAPES faz dos veículos onde estes artigos são publicados, chamada de Qualis.

Além destas informações, foram usados documentos de requisitos iniciais já desenvolvidos anteriormente em outros projetos de graduação e iniciação científica, disponibilizados pelos alunos envolvidos, os \textit{stakeholders} do projeto. Feita a revisão de tais documentos, foram extraídas as informações pertinentes e alterações foram feitas conforme o escopo deste projeto. 

O C2D deve considerar, inicialmente, os critérios estabelecidos pelo PPGI/UFES, mas sendo também genérico o suficiente para permitir atualização destes critérios (caso mudem no futuro), podendo, assim, também ser aproveitado por outros PPGs. Para efeito de Minimundo, no entanto, consideram-se os critérios de publicação atuais do PPGI.

O PPGI define atualmente 4 critérios: (a) credenciamento (ingresso) no programa; (b) recredenciamento (manutenção) no programa; (c) categorização como colaborador ou permanente; e (d) habilitação para orientação no doutorado. Tais critérios envolvem vários aspectos, como: ter plano de trabalho aprovado, ter lecionado disciplinas, ter concluído orientações, ter vínculo funcional-administrativo e, por fim, atender a requisitos mínimos de publicação.

A princípio, o \textbf{C2D deve avaliar apenas os requisitos de publicação}. Neste contexto, os critérios (a) e (b) utilizam um requisito de publicação, enquanto o critério (d) utiliza um outro requisito de publicação. Em 2017, o requisito para ingresso/permanência no programa é somar 20 pontos de publicação no último biênio, enquanto o requisito para orientar no doutorado é somar 25 pontos no biênio, com 10 pontos advindos de publicações em periódicos no triênio.

\subsection{Cálculo de Pontuação}
\label{sec-minimundo:calculo-pontuacao}

Tal pontuação é calculada de acordo com a classificação do veículo de publicação (conferência ou periódico) no Qualis da CAPES. Atualmente, a pontuação é dada seguindo as regras abaixo:\footnote{\url{http://www.informatica.ufes.br/pt-br/pos-graduacao/PPGI/credenciamento-de-docentes}.}


\begin{itemize}
	\item Publicações em periódicos com Qualis B1 ou superior acumulam 20 pontos;
	\item Publicações em periódicos com Qualis B2 ou B3 acumulam 10 pontos;
	\item Publicações em periódicos com Qualis B4 ou B5 acumulam 2 pontos;
	\item Publicações em conferências com Qualis B1 ou superior acumulam 10 pontos;
	\item Publicações em conferências com Qualis B2 ou B3 acumulam 5 pontos.
\end{itemize}

Para computação dos pontos será utilizado o Qualis do ano da publicação ou o mais recente divulgado (para publicações sem Qualis deve-se usar os índices JCR\footnote{\url{http://www.periodicos.ufscar.br/noticias/o-que-e-o-journal-citation-reports-jcr}} e \textit{h-index}\footnote{\url{https://pt.wikipedia.org/wiki/Índice_h}} com parametrização equivalente aos extratos Qualis correspondentes). Desta forma, o critério de credenciamento fica independente das mudanças que podem ocorrer no Qualis. Adicionalmente, o critério é aplicável a qualquer periódico e evento, independentemente de constar nas listas do Qualis. Mais detalhes podem ser vistos no site do PPGI: \url{http://www.informatica.ufes.br/pt-br/pos-graduacao/PPGI/credenciamento-de-docentes}.

\subsection{Fluxo de Trabalho no C2D}
\label{sec-minimundo:fluxo-de-trabalho-no-c2d}

Considerando que dados sobre membros do PPGI, suas publicações, respectivos fatores de impacto e classificação Qualis, etc. encontram-se já registrados em outros sistemas e documentos, o C2D deve funcionar de maneira mais automatizada possível, obtendo estes dados e armazenando em sua base de dados local, atualizando-os sempre que necessário. Então, temos o seguinte fluxo de trabalho para o C2D:

\begin{enumerate}
	\item Utilizando a função de cadastro presente no módulo núcleo (\textit{core}) do Marvin, a secretária do PPG realiza seu cadastro no sistema, informando seu nome, e-mail, CPF e senha para que possa administrar o sistema. Após acesso ao sistema, a secretária pode efetuar também o cadastro dos docentes no sistema. 
	
	\item A secretária cadastra manualmente os diferentes níveis de classificação do Qualis (A1, A2, B1, B2, B3, etc.), o sistema de pontuação vigente e os requisitos de publicação vigentes, podendo cadastrar quantos requisitos quiser (atualmente o PPGI usa dois, como descrito anteriormente). Para cada requisito, além do nome e da vigência (data de início e fim), é possível associar a cada nível de classificação uma pontuação nacional e uma internacional;
	
	\item \label{item-minimundo-fluxo-qualis} A secretária pede ao C2D que extraia informações de classificação Qualis de conferências e periódicos. Atualmente, a classificação de conferências pode ser encontrada em uma tabela no documento PDF disponível no site da CAPES.\footnote{\url{http://capes.gov.br/images/documentos/Qualis_periodicos_2016/Qualis_conferencia_ccomp.pdf}}
	Já a classificação de periódicos encontra-se na plataforma Sucupira,\footnote{\url{https://sucupira.capes.gov.br/sucupira/}.} clicando na figura do Qualis e preenchendo o formulário de consulta. Note que nem o documento do Qualis Conferências, nem o sistema Sucupira indicam se o veículo é nacional ou internacional, portanto tal informação deve ser completada manualmente;
	
	\item \label{item-minimundo-fluxo-lattes} A última extração de dados que a secretária pode pedir ao C2D para fazer é recuperar as publicações dos docentes a partir de seus Currículos Lattes;
	
	\item Com os docentes, critérios, qualificação e publicações cadastrados (atualizados), a secretária finalmente solicita ao C2D que calcule a pontuação de publicação de cada docente e informa, para cada um deles, se atendem aos requisitos cadastrados (no caso do PPGI, os dois requisitos que são avaliados atualmente).
	
	%Tanto quanto possível, a extração de informações deve ser automática e configurável (ou seja, caso os dados mudem de formato seja possível mudar a configuração da extração e continuar extraindo dados sem ter que implementar novamente a funcionalidade em código). Quando não for possível extrair os dados automaticamente, a secretária deve obter manualmente um arquivo e fazer upload no sistema. Quando os dados extraídos automaticamente não forem confiáveis, deve-se também fazer uma extração semi-automática, apresentando os dados extraídos e pedindo para que sejam confirmados/completados.
	
\end{enumerate}

\subsection{Atualização de Dados}
\label{sec-minimundo:atualizacao-de-dados}

O cadastro de docentes (passo 2 acima) pode ser repetido de tempos em tempos, quando a secretária assim desejar. Esta funcionalidade se encontra no módulo núcleo (\textit{core}) do Marvin, onde podem ser visualizados os docentes atualmente cadastrados, docentes a manter como estão (não muda nada), docentes que mudaram de tipo, novos docentes a cadastrar (presentes na lista de docentes do site mas não no cadastro) e docentes a remover do programa (presentes no cadastro mas não na lista de docentes).

O cadastro da classificação Qualis (passo~\ref{item-minimundo-fluxo-qualis}) também deve ser repetido de tempos em tempos, fazendo uma atualização semi-automática similar à descrita para os docentes. É importante ressaltar que o veículo deve ser cadastrado uma só vez e ter associado a ele múltiplas classificações, cada uma com seu(s) ano(s) de vigência.

A extração de dados do Lattes (passo~\ref{item-minimundo-fluxo-lattes}) é limitada pelo fato deste processo utilizar \textit{captcha} para impedir a obtenção automática dos currículos. Neste caso, a solução utilizada emprega a extração manual dos dados: a secretária obtém o currículo em formato XML manualmente e faz o \textit{upload} do mesmo no sistema. Além disso, os dados de publicação nos currículos são informados pelos próprios docentes e não há garantia que o nome dos veículos cadastrados por eles em seus currículos bate com o nome dos veículos obtidos na lista de conferências / periódicos do Qualis. O C2D deve tentar fazer o máximo possível para reconhecer automaticamente os veículos de cada publicação, utilizando a abordagem semi-automática sempre que necessário.


\section{Análise de Objetivos}
\label{sec-analise-objetivo}

Com o escopo definido e o minimundo descrito, foi construído um modelo de objetivos para o módulo C2D do Marvin, com o intuito de mapear os principais objetivos e possíveis tarefas no sistema, auxiliando, assim, o processo de análise dos requisitos. A Figura~\ref{modelo-objetivo} representa o modelo de objetivos, o qual foi criado utilizando a ferramenta online piStar\footnote{\url{http://www.cin.ufpe.br/~jhcp/pistar/}} para modelagem com a linguagem iStar 2.0.

\begin{figure}[h!]
	\centering
	\includegraphics[width=\textwidth]{figuras/requisitos/modelo_objetivo/modelo-objetivo.png}
	\caption{Modelo de Objetivos do Módulo C2D.}
	\label{modelo-objetivo}
\end{figure}

Considerou-se o principal objetivo (\textit{goal}) do sistema \texttt{Ter dados de docentes gerenciados}. Ligados a este objetivo existem outros objetivos secundários, os quais serão explicados a seguir.

Para o subojbetivo \texttt{Ter dados de venue gerenciados}, tem-se a tarefa (\textit{task}) de classificar veículos de acordo com o recurso (\textit{resource}) associado, que neste caso, é um Qualis. Outra tarefa associada a este objetivo é a criação de um sistema de pontuação, onde o recurso utilizado é um quadro de pontuação cujo conteúdo são os valores de cada Qualis.

Prosseguindo, para o subobjetivo \texttt{Ter dados de publicação gerenciados} tem-se a  tarefa de classificar as publicações, de acordo com a sua categoria (conferência ou periódico) e com as informações contidas nos arquivos .csv relacionados.

Para o subobjetivo \texttt{Ter dados de pesquisador gerenciados}, é preciso ter o cadastro deste efetuado. No cadastro de docente, é necessário incluir o seu currículo Lattes. A outra tarefa é calcular sua pontuação. Quando o cálculo passa a ser feito por meio do sistema, o tempo gasto nesta tarefa é reduzido. Tais fatos são representados no modelo pelo construto \textit{Quality}.
O mesmo ocorre na autenticação do administrador. Esta tarefa faz com que o acesso ao sistema seja restrito, portanto mais seguro.

A relação entre a secretária e o C2D é representada pela tarefa de manipular todos os dados através do gerenciamento do sistema, como mostrado na Figura~\ref{modelo-objetivo}. O modelo auxilia na análise do sistema, mapeando as tarefas que devem ocorrer para que os objetivos e intenções dos atores sejam satisfeitos.   


\section{Diagrama de Casos de Uso}
\label{sec-requisitos-casos-de-uso}
	
Nesta seção são descritos os casos de uso relacionados ao módulo C2D e atores envolvidos na aplicação. Apesar do Marvin envolver diferentes atores no contexto de uma universidade, o módulo C2D foca na secretária do PPG. A Figura~\ref{fig-caso_uso} mostra o diagrama de casos de uso para o C2D.

\begin{figure}[h!]
	\centering
	\includegraphics[width=\textwidth]{figuras/requisitos/casos_uso/casos-uso-secretaria.png}
	\caption{Diagrama de Casos de Uso do Módulo C2D.}
	\label{fig-caso_uso}
\end{figure}

Como descrito na Seção~\ref{sec-minimundo:fluxo-de-trabalho-no-c2d}, a secretária realiza seu cadastro logo que o sistema é instalado, informando seu nome, e-mail, CPF e a uma senha de acesso. Após isso, é necessário cadastrar os docentes do PPG informando seus principais dados, como nome completo, CPF, e-mail, data de nascimento e link para currículo Lattes. Essas funcionalidades estão implementadas no módulo \textit{core} (núcleo). No C2D, a secretária tem a tarefa de indicar quais são os docentes pertencentes ao PPG cadastrado, processo representado pelo caso de uso \textbf{Cadastrar PPG}. 

O caso de uso \textbf{Cadastrar Níveis de Qualis} representa o cadastro manual do Qualis feito pela secretária, enquanto \textbf{Cadastrar Pontuação Vigente} representa o processo de atualização de Qualis, pois seus valores podem ser alterados. Vale ressaltar que este processo se refere à pontuação vigente estabelecida pelo PPGI e não por meio do Qualis definido pela CAPES.

Assim, com o cadastro de docentes realizado e Qualis definidos, são descritos agora casos de uso envolvendo os dados de publicação: para as publicações, definidas como \textit{Journal} ou \textit{Conference}, existem arquivos (tabelas) com definições de pontuação e qualificação, de acordo com o veículo informado. Para o caso de uso \textbf{Extrair Informação de Qualificação}, o sistema C2D faz a leitura desses dados e os armazena para que possam ser usados. Para \textbf{Recuperar Publicação de Docente}, o sistema associa o código Lattes do currículo de um docente (fluxo descrito na Seção~\ref{sec-minimundo:fluxo-de-trabalho-no-c2d}).
Para o caso de uso \textbf{Calcular Pontuação de Docente}, a secretária informa o período e docente(s) que deseja calcular a pontuação, obtendo tal informação do sistema. 


\section{Diagrama de Classes}
\label{sec-requisitos-diagrama-de-classes}

A Figura~\ref{fig-diagrama-classes} apresenta o diagrama de classes do módulo C2D. As classes e suas associações são descritas a seguir.

\begin{figure}[h!]
	\centering
	\includegraphics[width=\textwidth]{figuras/requisitos/diagrama_classes/diagrama_classes.png}
	\caption{Diagrama de Classes do Módulo C2D.}
	\label{fig-diagrama-classes}
\end{figure}

As informações de docentes as quais a secretária irá administrar são associadas à classe \texttt{Academic}, pertencente ao módulo núcleo (\textit{core}) do Marvin.

Um acadêmico (\texttt{Academic}) pode ser cadastrado sem possuir publicações associadas ao seu nome, ao passo que uma publicação (\texttt{Publication}) deve ser registrada com o nome do seu autor correspondente. Publicações são publicadas em um determinado veículo (\texttt{Venue}) e este, por sua vez, pode ter várias publicações associadas ou nenhuma associação. Para o sistema C2D, foi feita somente a distinção de veículos do tipo conferência (\textit{Conference}) e periódico (\textit{Journal}), armazenados na classe \texttt{VenueCategory}.

Cada veículo é também classificado por Qualis, referentes ao ano vigente, definido em \texttt{Qualification}.
\texttt{Qualis} pode ser do tipo A1, A2, B1, B2, B3, B4, B5 e C. Estes itens são cadastrados com seus respectivos níveis. Contudo, o sistema permite que novos níveis de Qualis sejam cadastrados posteriormente.

Assim, a secretária pode solicitar ao C2D que extraia os dados de acadêmico necessários para o cálculo da pontuação segundo suas publicações e de acordo com um sistema de pontuação (\texttt{ScoreSystem}), definindo o período que se deseja calcular a pontuação (\texttt{Score}).
