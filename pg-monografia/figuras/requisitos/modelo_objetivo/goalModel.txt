{
  "actors": [
    {
      "id": "a9b1c138-bc79-4574-9688-5e565a23c336",
      "text": "C2D - UFES",
      "type": "istar.Actor",
      "x": 71,
      "y": 156,
      "nodes": [
        {
          "id": "e979842d-660c-4712-9755-9d18b5e27ef9",
          "text": "Ter dados de docentes gerenciados",
          "type": "istar.Goal",
          "x": 358,
          "y": 199
        },
        {
          "id": "d977d444-ca34-4d33-9c18-ec85ac553c4b",
          "text": "Ter dados de venue gerenciados",
          "type": "istar.Goal",
          "x": 213,
          "y": 218
        },
        {
          "id": "5f306294-e30f-42ae-9ab4-8e9a865474c8",
          "text": "Ter dados de pesquisador gerenciados",
          "type": "istar.Goal",
          "x": 470,
          "y": 268
        },
        {
          "id": "ab34d0de-94c3-4689-921e-ad366eca9772",
          "text": "Ter dados de publica��o gerenciados",
          "type": "istar.Goal",
          "x": 337,
          "y": 294
        },
        {
          "id": "b6294350-ebca-457f-8fbd-c84cc13c93ff",
          "text": "Seguran�a",
          "type": "istar.Quality",
          "x": 660,
          "y": 318
        },
        {
          "id": "53758715-e5cf-4ce6-a747-bdfb1e08ed34",
          "text": "Autenticar administrador",
          "type": "istar.Task",
          "x": 560,
          "y": 210
        },
        {
          "id": "558d84d3-3d7f-4861-a6e0-0b74a7c3a7a2",
          "text": "Classificar venue",
          "type": "istar.Task",
          "x": 103,
          "y": 298
        },
        {
          "id": "0837d415-92a3-48df-aac1-2a1179e10434",
          "text": "Calcular pontua��o de docente",
          "type": "istar.Task",
          "x": 548,
          "y": 341
        },
        {
          "id": "5a50ce8a-0cf4-4755-94f6-73d3490a4db2",
          "text": "Classificar publica��o",
          "type": "istar.Task",
          "x": 319,
          "y": 374
        },
        {
          "id": "6f29a5c9-1d23-4eb7-b7a3-2029d06f0e3b",
          "text": "Redu��o de tempo",
          "type": "istar.Quality",
          "x": 596,
          "y": 456
        },
        {
          "id": "530c33aa-80e6-4fc7-9d6a-e01b4b28c105",
          "text": "Automatiza�� o do processo",
          "type": "istar.Quality",
          "x": 504,
          "y": 482
        },
        {
          "id": "781e90d5-e0b6-441e-9674-570a1a08ce1d",
          "text": "Cadastrar pesquisador",
          "type": "istar.Task",
          "x": 435,
          "y": 345
        },
        {
          "id": "132a6dc1-5bcc-49ad-a18c-993a3eafa215",
          "text": "Curr�culo Lattes",
          "type": "istar.Resource",
          "x": 437,
          "y": 408
        },
        {
          "id": "1836eea7-f0ca-473f-8c6a-555b5702d82e",
          "text": "Arquivo csv Journal",
          "type": "istar.Resource",
          "x": 260,
          "y": 483
        },
        {
          "id": "f69b0725-aea8-4c59-b1d4-41bbd9d36d52",
          "text": "Arquivo csv Conference",
          "type": "istar.Resource",
          "x": 367,
          "y": 481
        },
        {
          "id": "916ae50a-0c31-48b1-9e34-4a9815ee96cd",
          "text": "Criar sistema de pontua��o",
          "type": "istar.Task",
          "x": 219,
          "y": 317
        },
        {
          "id": "5fa0fc40-83be-4859-94b5-a8b76dbfac04",
          "text": "Qualis",
          "type": "istar.Resource",
          "x": 94,
          "y": 379
        },
        {
          "id": "ddfe0554-c3ef-4c71-9cdb-cbe14c8ebced",
          "text": "Quadro de pontua��o",
          "type": "istar.Resource",
          "x": 211,
          "y": 409
        }
      ]
    },
    {
      "id": "53dd34b0-b729-47d7-b3f8-5c5298be6e76",
      "text": "ADM Sistema",
      "type": "istar.Actor",
      "x": 792,
      "y": 135,
      "nodes": []
    }
  ],
  "dependencies": [
    {
      "id": "850bd76c-4897-41fa-8f49-d7d60109aa48",
      "text": "Manipular dados de doc entes-PPGI",
      "type": "istar.Task",
      "x": 864,
      "y": 211,
      "source": "53dd34b0-b729-47d7-b3f8-5c5298be6e76",
      "target": "e979842d-660c-4712-9755-9d18b5e27ef9"
    }
  ],
  "links": [
    {
      "id": "bf082e44-2c55-4ce9-96c9-59699eb5bdb1",
      "type": "istar.AndRefinementLink",
      "source": "d977d444-ca34-4d33-9c18-ec85ac553c4b",
      "target": "e979842d-660c-4712-9755-9d18b5e27ef9"
    },
    {
      "id": "fc3f1844-ec6c-4188-87b5-3dbe839d1699",
      "type": "istar.AndRefinementLink",
      "source": "5f306294-e30f-42ae-9ab4-8e9a865474c8",
      "target": "e979842d-660c-4712-9755-9d18b5e27ef9"
    },
    {
      "id": "f731f0ce-51e7-4d71-b7b7-5c2d64491a52",
      "type": "istar.AndRefinementLink",
      "source": "ab34d0de-94c3-4689-921e-ad366eca9772",
      "target": "e979842d-660c-4712-9755-9d18b5e27ef9"
    },
    {
      "id": "a50e9cab-8c4e-4dde-8a16-d7a0ecab1395",
      "type": "istar.OrRefinementLink",
      "source": "5a50ce8a-0cf4-4755-94f6-73d3490a4db2",
      "target": "ab34d0de-94c3-4689-921e-ad366eca9772"
    },
    {
      "id": "ac253595-2d59-4696-9373-7cd4c1e235f6",
      "type": "istar.ContributionLink",
      "source": "53758715-e5cf-4ce6-a747-bdfb1e08ed34",
      "target": "b6294350-ebca-457f-8fbd-c84cc13c93ff",
      "label": "make"
    },
    {
      "id": "333bd7cf-afb6-43db-a0fb-803570c6627e",
      "type": "istar.ContributionLink",
      "source": "0837d415-92a3-48df-aac1-2a1179e10434",
      "target": "530c33aa-80e6-4fc7-9d6a-e01b4b28c105",
      "label": "make"
    },
    {
      "id": "cf67122b-c48a-4604-8380-df5113731dfb",
      "type": "istar.ContributionLink",
      "source": "0837d415-92a3-48df-aac1-2a1179e10434",
      "target": "6f29a5c9-1d23-4eb7-b7a3-2029d06f0e3b",
      "label": "help"
    },
    {
      "id": "58fcb867-aabc-4d2c-a576-8c901c763dfb",
      "type": "istar.DependencyLink",
      "source": "53dd34b0-b729-47d7-b3f8-5c5298be6e76",
      "target": "850bd76c-4897-41fa-8f49-d7d60109aa48"
    },
    {
      "id": "0e392194-71e4-4b97-8867-ca6793544ea6",
      "type": "istar.DependencyLink",
      "source": "850bd76c-4897-41fa-8f49-d7d60109aa48",
      "target": "e979842d-660c-4712-9755-9d18b5e27ef9"
    },
    {
      "id": "14247562-bf75-4b05-aa8f-6f256891763f",
      "type": "istar.AndRefinementLink",
      "source": "0837d415-92a3-48df-aac1-2a1179e10434",
      "target": "5f306294-e30f-42ae-9ab4-8e9a865474c8"
    },
    {
      "id": "69cbe408-3eec-4fb5-a386-baae2b6d0a69",
      "type": "istar.AndRefinementLink",
      "source": "781e90d5-e0b6-441e-9674-570a1a08ce1d",
      "target": "5f306294-e30f-42ae-9ab4-8e9a865474c8"
    },
    {
      "id": "07417bad-2848-48ca-9dd9-8bd55d5ed0f2",
      "type": "istar.NeededByLink",
      "source": "132a6dc1-5bcc-49ad-a18c-993a3eafa215",
      "target": "781e90d5-e0b6-441e-9674-570a1a08ce1d"
    },
    {
      "id": "3511a690-0823-4cb3-b6e3-735624ca532b",
      "type": "istar.NeededByLink",
      "source": "1836eea7-f0ca-473f-8c6a-555b5702d82e",
      "target": "5a50ce8a-0cf4-4755-94f6-73d3490a4db2"
    },
    {
      "id": "d8fae260-c5d6-47ec-b603-07badd5d2459",
      "type": "istar.NeededByLink",
      "source": "f69b0725-aea8-4c59-b1d4-41bbd9d36d52",
      "target": "5a50ce8a-0cf4-4755-94f6-73d3490a4db2"
    },
    {
      "id": "f5c0c28f-c347-4462-87f6-6c8bd4191aa1",
      "type": "istar.AndRefinementLink",
      "source": "558d84d3-3d7f-4861-a6e0-0b74a7c3a7a2",
      "target": "d977d444-ca34-4d33-9c18-ec85ac553c4b"
    },
    {
      "id": "8b436444-3fec-4472-873d-b1a117d2e103",
      "type": "istar.AndRefinementLink",
      "source": "916ae50a-0c31-48b1-9e34-4a9815ee96cd",
      "target": "d977d444-ca34-4d33-9c18-ec85ac553c4b"
    },
    {
      "id": "6071c26e-c8b9-4a58-bc33-7e5ed815b852",
      "type": "istar.AndRefinementLink",
      "source": "53758715-e5cf-4ce6-a747-bdfb1e08ed34",
      "target": "e979842d-660c-4712-9755-9d18b5e27ef9"
    },
    {
      "id": "1d2b7132-a440-4c23-8afc-e6d24727e45e",
      "type": "istar.NeededByLink",
      "source": "5fa0fc40-83be-4859-94b5-a8b76dbfac04",
      "target": "558d84d3-3d7f-4861-a6e0-0b74a7c3a7a2"
    },
    {
      "id": "8ea09d5e-0f21-4749-bb96-55f9c3339b99",
      "type": "istar.NeededByLink",
      "source": "ddfe0554-c3ef-4c71-9cdb-cbe14c8ebced",
      "target": "916ae50a-0c31-48b1-9e34-4a9815ee96cd"
    }
  ],
  "tool": "pistar.1.0.0",
  "istar": "2.0",
  "saveDate": "Wed, 18 Oct 2017 23:47:37 GMT",
  "diagram": {
    "width": 1975.5,
    "height": 1172
  }
}