\chapter{Fundamentação Teórica}
\label{sec-teorica}

O desenvolvimento de software, de fato, é uma atividade de suma importância para nossa geração. A utilização de computadores e outros dispositivos nas mais diversas áreas do conhecimento humano tem gerado uma crescente demanda por soluções computadorizadas. Buscando melhorar a qualidade dos produtos de software e aumentar a produtividade no processo de desenvolvimento, surgiu a Engenharia de Software~\cite{falboEngSoft}.

Segundo \citeonline{pressman2011engenharia},  a Engenharia de Software proporciona várias ferramentas para o desenvolvimento de um software de qualidade por meio da especificação, desenvolvimento e manutenção destes sistemas de software. Tudo isso é feito por meio de tecnologias e práticas de gerência de projetos e outras disciplinas, visando organização, produtividade e qualidade nesse processo de desenvolvimento~\cite{falboEngSoft}.
Sendo assim, podemos citar as seguintes atividades do processo de desenvolvimento de software: elicitação de requisitos (especificação e análise), projeto arquitetural, implementação, análise de riscos, definição de cronogramas, dentre outros.

Na Seção~\ref{sec-analise-req} é descrita a etapa de especificação e análise de requisitos. Na seção~\ref{sec-objetivos-model} é descrito de forma sucinta a linguagem iStar e seus principais construtos. Na Seção~\ref{sec-projeto-impl} é descrita a etapa de projeto e implementação, visando mostrar a importância dessas etapas no desenvolvimento de software. Na Seção~\ref{sec-webdev} são apresentados conceitos utilizados no desenvolvimento Web e, por fim, na Seção~\ref{sec-frameweb} é descrito o método \textit{FrameWeb}.



\section{Especificação e Análise de Requisitos}
\label{sec-analise-req}

A Engenharia de Requisitos é considerada uma das atividades mais importantes no desenvolvimento de software, pois é nela que são definidas as necessidades do cliente e os possíveis problemas e limitações do projeto.

O desenvolvimento da especificação de requisitos de um software é reconhecido como uma das bases das funcionalidades de um sistema. Estes requisitos são determinantes da qualidade do software,  dado que estudos empíricos mostraram que erros nos requisitos são as falhas mais comuns no ciclo de vida de um software, bem como as mais caras e custosas a corrigir~\cite{aurum2013managing}.

A atividade inicia com o levantamento de requisitos, onde são coletadas as necessidades dos usuários, informações de domínio, restrições, sistemas interligados, regulamentos etc. A criação de modelos segue em paralelo à atividade de análise, onde estes representam o o que o sistema faz. Esta atividade é conhecida como Modelagem Conceitual. Nesta etapa, o foco é o domínio do problema e não a solução técnica, linguagem a ser usada na implementação etc. Com essas informações, jé se pode avançar no processo de desenvolvimento de software~\cite{falboEngReq}.

A Figura \ref{fig-processo-req} representa o ciclo de vida do processo de Engenharia de Requisitos identificando suas principais atividades e dependências.

\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{figuras/processo-requisitos.png}
	\caption{Processo de Engenharia de Requisitos adaptado de \cite{kotonya1998requirements}.}
	\label{fig-processo-req}
\end{figure}

Pode-se dizer que os requisitos de um sistema incluem especificações dos serviços que o sistema deve prover, restrições sob as quais ele deve operar, propriedades gerais do sistema e restrições que devem ser satisfeitas no seu processo de desenvolvimento~\cite{falboEngReq}, sendo estes requisitos divididos em \textbf{requisitos funcionais}, \textbf{requisitos não funcionais} e \textbf{regras de negócio}, definidos segundo \citeonline{aurum2013managing} da seguinte forma:

\begin{itemize}
	\item Requisitos Funcionais: o que o sistema efetivamente deve fazer;
	\item Requisitos Não-funcionais: especificação de um determinado comportamento do sistema. Por exemplo: usabilidade, interoperabilidade e segurança;
	\item Regras de Negócio: usadas pela organização de forma a satisfazer os objetivos de negócio e clientes, segundo convenções definidas para o negócio.
\end{itemize}

\section{Análise de Objetivos}
\label{sec-objetivos-model}

A análise e modelagem de objetivos utilizando a linguagem iStar 2.0 (anteriormente i*) é uma tentativa de introduzir alguns aspectos de modelagem social e raciocínio em métodos de engenharia em sistemas de informação, especialmente no nível de requisitos. Diferente dos métodos tradicionais de análise de sistemas, em iStar tem-se a capacidade de reconhecimento de relações e intenções de atores no contexto social, focando em como as intenções dos atores (seja humano ou sistema) estão dispostas no domínio, podendo ser feito alterações ou reconfigurações das relações de modo a ajudar nos objetivos dos atores. ~\cite{Mylopoulos:1999:OGR:291469.293165}.
Para isso, iStar apresenta alguns construtos como:\textit{Actor, Agent, Role, Goal, Quality, Task, Resource}~\cite{DBLP:journals/corr/DalpiazFH16}. Alguns destes construtos são exemplificados na Figura~\ref{fig-istar} e explicados brevemente à seguir:

\begin{figure}
	\centering
	\includegraphics[width=0.4\textwidth]{figuras/construtos.png}
	\caption{Construtos da linguagem iStar 2.0.}
	\label{fig-istar}
\end{figure}

\begin{itemize}
	\item \textit{Actor}: entidade que possui objetivos e realiza ações para alcançá-los, exercendo o seu know-how;
	
	\item \textit{Goal}: representa o desejo intencional de um ator. Os detalhes de como o objetivo é satisfeito não é descrito pelo objetivo. Portanto, pode ser descrito através de refinamento em tarefas; 
	
	\item \textit{Quality}: é um objetivo que se diferencia do goal por seu critério de satisfação, que é subjetivo. Um \textit{quality} é considerado suficientemente satisfeito do ponto de vista do ator;
	
	\item \textit{Task}: uma ação ou um conjunto de ações (passo a passo) realizadas pelo ator. O detalhamento da tarefa pode ser obtido pela decomposição de tarefa em outros subelementos;
	
	\item \textit{Resource}: entidade física ou informacional usada pelo ator. Assume-se que o recurso está disponível ao ator que o utiliza.
	
\end{itemize}

Além disso, existem links para representação de relações, descritos à seguir:

\begin{itemize}
	\item \textit{OR-Refiniment link}: aponta para uma relação entre um fim e um meio que atinge esse fim. Um meio pode, por exemplo, ser uma tarefa ou um recurso e um fim pode ser um objetivo;
	
	\item \textit{AND-Refiniment link}: um objetivo pode ser decomposto em sub-objetivos e uma tarefa pode ser decomposta em quatro tipos de elementos: um subobjetivo, uma subtarefa, um recurso, e/ou um \textit{quality};
	
	\item \textit{Contribution link}: contribuições podem ser usadas para ligar qualquer um dos elementos a um \textit{quality}, para modelar a maneira que o elemento afeta a satisfação ou cumprimento do \textit{quali}ty;
	
	\begin{itemize}
		\item Make ($++$): a contribuição é positiva o suficiente para satisfazer o \textit{quality}; 
		\item Help ($+$): uma contribuição positiva parcial, não é suficiente por si só para satisfazer o \textit{quality};
		\item Hurt ($-$): uma contribuição negativa parcial, não é suficiente por si só para negar o \textit{quality};
		\item Break ($--$): a contribuição é negativa o suficiente para negar o \textit{quality}.
		
	\end{itemize}
\end{itemize}

Há uma ferramenta online, disponível em \url{http://www.cin.ufpe.br/~jhcp/pistar/}, que permite a criação de diagramas usando a linguagem iStar 2.0.


\section{Projeto e Implementação}
\label{sec-projeto-impl}

Nesta fase, define-se o projeto da arquitetura do sistema, onde é descrita a estrutura de nível mais alto da aplicação, identificando seus elementos ou componentes e suas dependências entre si.
A partir daí, é realizado um detalhaento desses elementos, até chegar no nível de unidades de implementação~\cite{falboProjeto}.

Inicialmente, o projeto é representado em um nível alto de abstração, focando a estrutura geral do sistema. Definida a arquitetura, o projeto passa a tratar do detalhamento de seus elementos. Esses refinamentos conduzem a representações de menores níveis de abstração, até se chegar ao projeto de algoritmos e estruturas de dados. Assim, independentemente do paradigma adotado, o processo de projeto envolve as seguintes atividades~\cite{falboProjeto}:

\begin{itemize}
	\item Projeto da Arquitetura do Software: visa definir os elementos estruturais do software e seus relacionamentos;

	\item Projeto dos Elementos da Arquitetura: visa projetar em um maior nível de detalhes cada um dos elementos estruturais definidos na arquitetura, o que envolve a decomposição de módulos em submódulos;
	
	\item Projeto Detalhado: tem por objetivo refinar e detalhar os elementos mais básicos da arquitetura do software (interfaces, processos e  estruturas de dados). Descreve-se como a comunicação entre os elementos da arquitetura será realizada (interfaces internas), a comunicação do sistema em desenvolvimento com outros sistemas (interfaces externas) e com as pessoas que vão utilizá-lo (interface com o usuário).

\end{itemize}


\section{Desenvolvimento Web}
\label{sec-webdev}

A tecnologia está fortemente presente em nosso cotidiano. Um bom exemplo disso são as aplicações web, executadas em computadores, tablets, smartphones etc, as quais podem ser utilizadas por quaisquer pessoas com acesso à internet. Com isso, torna-se importante o processo de desenvolvimento de aplicações web com características como manutenibilidade e escalabilidade.

Diferentemente de aplicações desktop, aplicações web são baseadas na arquitetura cliente/servidor. Nesse tipo de arquitetura, existe um servidor que atende às requisições de diversos clientes e de outros possíveis servidores. Enquanto que, esses clientes, fazem requisições para o servidor, através de \textit{browsers} (navegadores).


A Engenharia Web surgiu com a necessidade de lidar com o processo de desenvolvimento de aplicações e sistemas Web. O objetivo dessa área é gerenciar a diversidade e a complexidade dessas aplicações, evitando potenciais falhas que possam causar sérios problemas ao projeto. Pode-se dizer que a Engenharia Web é uma forma preventiva de desenvolver aplicações Web~\cite{ginige2001web}.

\section{O método FrameWeb}
\label{sec-frameweb}

FrameWeb é um método de projeto para construção de sistemas de informação Web (Web Information Systems – WIS's) baseado em \textit{frameworks}. O método define alguns tipos de frameworks que serão usados para construção da aplicação, definindo uma arquitetura básica para o WIS e propõe modelos de projeto que se aproximam da implementação do sistema usando esses frameworks.

O FrameWeb define uma arquitetura lógica baseada no padrão arquitetônico \textit{Service Layer} (Camada de Serviço)~\cite{fowler2002patterns}. As principais camadas do sistema são~\cite{salvatore2016alocaweb}:

\begin{itemize}
	\item\textbf{ Camada de Negócio \textit{(Business Tier)}:} responsável pelas funcionalidades relacionadas
	às regras de negócio da aplicação. Esta camada é particionada em duas: Lógica de Domínio (\textit{Domain}) e Lógica de Aplicação (\textit{Application});
	\item \textbf{Camada de Apresentação \textit{(Presentation Tier)}:} responsável por funcionalidades de interface com o usuário (incluindo as telas que serão apresentadas a ele). Esta Camada, segundo o padrão proposto, é também particionada em duas outras: Visão (\textit{View}) e Controle (\textit{Control});
	\item \textbf{Camada de Acesso a Dados \textit{(Data Access Tier)}:} responsável por funcionalidades
	relacionadas à persistência de dados.
\end{itemize}

As principais propostas do método são: (i) definição de uma arquitetura padrão que divide o sistema em camadas, de modo a se integrar bem com os \textit{frameworks} utilizados; (ii) proposta de um conjunto de modelos de projeto que trazem conceitos utilizados pelos \textit{frameworks} para esta fase do processo por meio da criação de uma linguagem específica de domínio que faz com que os diagramas fiquem mais próximos da implementação~\cite{souza-frameweb07,martins-mestrado16}. São quatro tipos de modelo propostos pelo FrameWeb:

\begin{itemize}
	\item \textbf{Modelo de Entidades:} é um diagrama de classes da UML que representa os objetos de	domínio do problema e seu mapeamento para a persistência em banco de dados relacional;
	
	\item \textbf{Modelo de Persistência:} é um diagrama de classes da UML que representa as classes DAO existentes, responsáveis pela persistência das instâncias das classes de domínio;
	
	\item \textbf{Modelo de Navegação:} é um diagrama de classe da UML que representa os diferentes componentes que formam a camada de Lógica de Apresentação, como páginas Web, formulários HTML e 
	\textit{controllers}.
	
	\item \textbf{Modelo de Aplicação:} é um diagrama de classes da UML que representa as classes de	serviço, que são responsáveis pela codificação dos casos de uso, e suas dependências.
\end{itemize}
